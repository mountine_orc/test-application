<?php
namespace Controller;

use Core\AbstractController;
use Model\User;

class SignupController extends AbstractController
{
    function __construct()
    {
        $this->user = new User;
        parent::__construct();
    }
    
    function indexAction()
    {
        $this->view->render("signup");
    }
    
    //AJAX method
    function addAction()
    {
        //TODO: check $_POST data and print errors if exist
        if ($_SESSION['code'] != $_POST['code']){
            echo "Wrong activation code";
        }
        else{
            if ($this->user->addNew($_POST["name"], $_POST["email"], md5($_POST["password"]), $_POST["phone"])){
                echo "TRUE";
            }
            else {
                echo "FALSE";
            }
        }
        die();
    }
    
    //AJAX method
    function sendcodeAction()
    {
        if ($this->user->isPhoneDuplicate($_POST["phone"])){
            echo "This phone is already registered";
        }else{
            $code = substr(md5(uniqid(mt_rand(), true)) , 0, 5);
            $_SESSION['code'] = $code;
            //TODO: Code sending using curl etc.:
            echo "Activation code sending is emulated. Phone number - +38".$_POST["phone"].", code is - $code";
        }
    }
}