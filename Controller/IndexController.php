<?php
namespace Controller;

use Core\AbstractController;
use Model\User;

class IndexController extends AbstractController{
    function indexAction()
    {
        $user = new User;
        $result   = $user->getLastUsers(5);
    
        $this->view->render("index", array("result"=>$result));
    }
}