<form>
    <div class="form-group">
        <label for="email">Your name:</label> <input type="name" class="form-control" name="name" id="name" placeholder="Input your name">
        <label for="email">Your e-mail:</label> <input type="email" class="form-control" name="email" id="email" placeholder="Input your e-mail">
        <label for="phone">Your phone:</label> +38<input type="tel" class="form-control" name="phone" id="phone" placeholder="Input your phone number"> 
        <small id="phoneHelpBlock" class="form-text text-muted">
            Only digits. Ex.: 0671234567. We will send you sms with activation code
        </small>
        <div class="form-group col-md-6">
                <button  class="btn btn-primary" id="sendCodeButton">Send code</button>
            </div>
        <label for="password">Your password:</label> <input type="password" class="form-control" name="password" id="password" placeholder="Input your password">                
        <small id="passwordHelpBlock" class="form-text text-muted">
            Password between 6 to 20 characters which contain at least one numeric digit, one uppercase, and one lowercase letter
        </small>
        <label for="password2">Repeat your password:</label> <input type="password" class="form-control" name="password2" id="password2" placeholder="Repeat your password">
        <label for="code">Type activation code:</label> <input type="text" class="form-control" name="code" id="code" placeholder="Type your code from SMS"> 
    </div>
    <button  class="btn btn-primary" id="signupButton" disabled>Signup</button>
</form>

<script>

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isPhone(phone) {
    var regex = /^([0-9]{10})+$/;
    return regex.test(phone);
}

function isGoodPassword(password) {
    var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    return regex.test(password);
}

$(document).ready(function(){                          
    $('#signupButton').click(function(){
        
        if ($( "#phone" ).val().trim() == "" || $( "#name" ).val().trim() == "" || $( "#email" ).val().trim() == "" || $( "#password" ).val().trim()=="" || $( "#code" ).val().trim()=="") {
            alert("All field must be filled");
            return false;
        };
        
        if (!isEmail($( "#email" ).val())) {
            alert("email is not valid");
            return false;
        };
        
        if (!isPhone($( "#phone" ).val())) {
            alert("phone is not valid");
            return false;
        };
        
        if (!isGoodPassword($( "#password" ).val())) {
            alert("Password between 6 to 20 characters which contain at least one numeric digit, one uppercase, and one lowercase letter");
            return false;
        };
        
        if ($( "#password" ).val() != $( "#password2" ).val()) {
            alert("Passwords isn't equal");
            return false;
        };
        
        $.ajax({
            url: "/signup/add",
            type: "POST",
            data: {
                name: $( "#name" ).val(),
                email: $( "#email" ).val(),
                password: $( "#password" ).val(),
                phone: $( "#phone" ).val(),
                code: $( "#code" ).val(),
            },
            success: function( data ) {
                if (data == "TRUE") {
                   alert("Done!");
                   window.location.href = "/";
                }
                else
                    alert("Can't add new user - "+data);
            }
        });
        
        return false;
    });
    
    $('#sendCodeButton').click(function(){
        if (!isPhone($( "#phone" ).val())) {
            alert("phone is not valid");
            return false;
        };
        
        $.ajax({
            url: "/signup/sendcode",
            type: "POST",
            data: {
                phone: $( "#phone" ).val(),
            },
            success: function( data ) {
                $('#signupButton').prop('disabled', false);
                alert(data);
            }
        });
        return false;
    })
});


</script>