<?php
namespace Model
{
    use Core\Db;
            
    class User
    {
        function __construct()
        {
            $dataBase = new Db;
            $this->db = $dataBase->connect();
        }
        
        function getLastUsers($limit = 1)
        {
            $stmt = $this->db->prepare('SELECT * FROM user ORDER BY id DESC LIMIT ?');
            $stmt->execute(array($limit));
            $result = $stmt->fetchAll();
            
            return $result;
        }
        
        function isPhoneDuplicate($phone)
        {
            $stmt = $this->db->prepare('SELECT * FROM user WHERE phone = ?');
            $stmt->execute(array($phone));
            $result = $stmt->fetchAll();
            
            return count($result);
        }
        
        function addNew($name, $email, $password, $phone)
        {
            //TODO: make worning on data duplication: phone, email
            $stmt = $this->db->prepare('INSERT INTO user(name, email, password, phone, registrationdate) values(?, ?, ?, ?, DATETIME())');
            if ($stmt->execute(array($name, $email, $password, $phone)))
                return TRUE;
            else
                return FALSE;
        }
        
    }
}